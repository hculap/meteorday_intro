Template.mainMenu.events({
    'click .js-logout': function (e) {
        e.preventDefault();
        Meteor.call('reset');
        Meteor.logout(function () {
            Router.go('/');
        });
    }
});