
//////////////////////////////////////////////////////////
// index view controller
//////////////////////////////////////////////////////////
indexViewController = RouteController.extend({
    data: function () {
        if (Meteor.isClient) {
            Session.set('siteTitle', 'Vazco SlideDeck!');
        };
    }
});

//////////////////////////////////////////////////////////
// slideshow view controller
//////////////////////////////////////////////////////////
slideshowViewController = RouteController.extend({
    data: function () {
        if (Meteor.isClient) {
            Session.set('siteTitle', 'Vazco SlideDeck! - Slideshow!');
        };
    }
});

//////////////////////////////////////////////////////////
// login view controller
//////////////////////////////////////////////////////////
loginViewController = RouteController.extend({
    data: function () {
        if (Meteor.isClient) {
            Session.set('siteTitle', 'Vazco SlideDeck! - Admin Login!');
        };
    }
});